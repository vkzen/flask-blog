$(function() {
    $.ajax({
        url: '/get_blog',
        type: 'GET',
        success: function(res) {
            var blogObj = JSON.parse(res);
            $('#list_template').tmpl(blogObj).appendTo('#ulist');
        },
        error: function(error) {
            console.log(error);
        }
    });
})

function Edit(elm) {
    localStorage.setItem('editId',$(elm).attr('data-id'));
    $.ajax({
        url: '/get_blog_id',
        data: {
            id: $(elm).attr('data-id')
        },
        type: 'POST',
        success: function(res) {
            var data = JSON.parse(res);
            $('#editTitle').val(data[0]['Title']);
            $('#editDescription').val(data[0]['Description']);
            $('#imgUpload').attr('src',data[0]['FilePath']);
            if(data[0]['Private'] == "1")
                $('#chkPrivate').attr('checked', 'checked');
            if(data[0]['Done'] == "1")
                $('#chkDone').attr('checked', 'checked');
            $('#editModal').modal();
        },
        error: function(error) {
            console.log(error);
        }
    });
}
function get_blogs() {
    $.ajax({
        url: '/get_blog',
        type: 'GET',
        success: function(res) {
            var blogObj = JSON.parse(res);
            $('#ulist').empty();
            $('#list_template').tmpl(blogObj).appendTo('#ulist');
        },
        error: function(error) {
            console.log(error);
        }
    });
}

function ConfirmDelete(elm) {
    localStorage.setItem('deleteId', $(elm).attr('data-id'));
    $('#deleteModal').modal();
}

function Delete() {
    $.ajax({
        url : '/delete_item',
        data : {id:localStorage.getItem('deleteId')},
        type : 'POST',
        success: function(res){
            var result = JSON.parse(res);
            if(result.status == 'OK'){
                $('#deleteModal').modal('hide');
                get_blogs();
            }
            else{
                alert(result.status);
            }
        },
        error: function(error){
            console.log(error);
        }
    });
}
$(function() {
    $('#btnUpdate').click(function() {
        if ($('#chkPrivate').is(':checked'))
            _private = "1";
        else
            _private = "0";
        if ($('#chkDone').is(':checked'))
            _done = "1";
        else
            _done = "0";
        $.ajax({
            url: '/update_item',
            data :
            {title:$('#editTitle').val(),description:$('#editDescription').val(),id:localStorage.getItem('editId'), filepath: $('#filePath').val(), 'private':_private, done: _done},
            type: 'POST',
            success: function(res) {
                $('#editModal').modal('hide');
                get_blogs();
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
})
