from flask import Flask, render_template, request, json, session, redirect
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import os
import uuid

mysql = MySQL()
app = Flask(__name__)

app.config['MYSQL_DATABASE_USER'] = 'flask'
app.config['MYSQL_DATABASE_PASSWORD'] = 'xcv!@#'
app.config['MYSQL_DATABASE_DB'] = 'FlaskBlog'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.secret_key = b'p\xc9~\x07\n\x10\xd4\xfc\xda\xb6\x80\x01\xb4\x01\xcae,\xd9Q\xf64\xfa\xa8n'
app.config['UPLOAD_FOLDER'] = 'static/uploads'

mysql.init_app(app)

@app.route("/")
def main():
    return render_template('index.html')

@app.route("/home")
def home():
    if session.get('user'):
        return render_template('home.html')
    else:
        return render_template('error.html', error = 'Unauthorize Access')

@app.route("/signup")
def signup():
    return render_template('signup.html')

@app.route("/signin")
def signin():
    return render_template('signin.html')

@app.route('/signup_post', methods=['POST'])
def signup_post():
    try:
        _name = request.form['inputName']
        _email = request.form['inputEmail']
        _password = request.form['inputPassword']

        # validate the received values
        if _name and _email and _password:

            conn = mysql.connect()
            cursor = conn.cursor()
            _hashed_password = generate_password_hash(_password)
            cursor.callproc('sp_createUser',(_name,_email,_hashed_password))
            data = cursor.fetchall()

            if len(data) is 0:
                conn.commit()
                return json.dumps({'message':'User created successfully !'})
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter the required fields</span>'})
    except Exception as e:
        return json.dumps({'error': str(e)})
    finally:
        cursor.close()
        conn.close()

@app.route('/signin_post', methods=['POST'])
def signin_post():
    try:
        _username = request.form['inputEmail']
        _password = request.form['inputPassword']

        # connect to mysql
        con = mysql.connect()
        cursor = con.cursor()
        cursor.callproc('sp_validateLogin',(_username,))
        data = cursor.fetchall()
        if len(data) > 0:
            if check_password_hash(str(data[0][3]),_password):
                session['user'] = data[0][0]
                return redirect('/home')
            else:
                return render_template('error.html',error = 'Wrong Email address or Password.')
        else:
            return render_template('error.html',error = 'Wrong Email address or Password.')

    except Exception as e:
        return render_template('error.html',error = str(e))
    finally:
        cursor.close()
        con.close()

@app.route('/logout')
def logout():
    session.pop('user', None)
    return redirect('/')

@app.route('/add')
def add():
    return render_template('add.html')

@app.route('/add_post', methods=['POST'])
def add_post():
    if session.get('user'):
        _title = request.form['inputTitle']
        _description = request.form['inputDescription']
        _user = session.get('user')
        if request.form.get('filePath') is None:
            _filePath = ''
        else:
            _filePath = request.form.get('filePath')


        if request.form.get('private') is None:
            _private = 0
        else:
            _private = 1

        if request.form.get('done') is None:
            _done = 0
        else:
            _done = 1

        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.callproc('sp_addBlog', (_title, _description, _user, _filePath, _private, _done))
        data = cursor.fetchall()
        if len(data) is 0:
            conn.commit()
            return redirect('/home')
        else:
            return render_template('error.html', error='An error occurred!')

@app.route('/get_blog')
def get_blog():
    try:
        if session.get('user'):
            _user = session.get('user')

            con = mysql.connect()
            cursor = con.cursor()
            cursor.callproc('sp_GetBlogByUser', (_user,))
            blogs = cursor.fetchall()

            blogs_dict = []
            for blog in blogs:
                blog_dict = {
                        'Id': blog[0],
                        'Title': blog[1],
                        'Description': blog[2],
                        'Date': blog[4]}
                blogs_dict.append(blog_dict)
            return json.dumps(blogs_dict)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html', error=str(e))

@app.route('/get_blog_id',methods=['POST'])
def get_blog_id():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_GetBlogById', (_id, _user))
            result = cursor.fetchall()
            blog = []
            #blog.append({'Id':result[0][0],'Title':result[0][1],'Description':result[0][2]})
            blog.append({'Id':result[0][0],'Title':result[0][1],'Description':result[0][2],'FilePath':result[0][3],'Private':result[0][4],'Done':result[0][5]})
            return json.dumps(blog)
        else:
            return render_template('error.html', error = 'Unauthorized Access')
    except Exception as e:
        return render_template('error.html',error = str(e))

@app.route('/update_item',methods=['POST'])
def update():
    try:
        if session.get('user'):
            _user = session.get('user')
            _title = request.form['title']
            _description = request.form['description']
            _blog_id = request.form['id']
            _filepath = request.form['filepath']
            _private = request.form['private']
            _done = request.form['done']

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_updateBlog', (_title, _description, _blog_id,
                _user, _filepath, _private, _done))
            data = cursor.fetchall()
            if len(data) is 0:
                conn.commit()
                return json.dumps({'status': 'OK'})
            else:
                return json.dumps({'status': 'ERROR'})
    except Exception as e:
        return json.dumps({'status': 'Unauthorized access'})
    finally:
        cursor.close()
        conn.close()

@app.route('/delete_item', methods=['POST'])
def delete():
    try:
        if session.get('user'):
            _id = request.form['id']
            _user = session.get('user')

            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.callproc('sp_deleteBlog',(_id, _user))
            result = cursor.fetchall()

            if len(result) is 0:
                conn.commit()
                return json.dumps({'status': 'OK'})
            else:
                json.dumps({'status', 'An Error ocurred'})
        else:
            return render_template('error.html',error = 'Unauthorized Access')
    except Exception as e:
        return json.dumps({'status': str(e)})
    finally:
        cursor.close()
        conn.close()




@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['file']
        extension = os.path.splitext(file.filename)[1]
        f_name = str(uuid.uuid4()) + extension
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], f_name))
    return json.dumps({'filename':f_name})


if __name__ == "__main__":
    app.run()
